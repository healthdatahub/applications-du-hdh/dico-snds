#!/usr/bin/env bash
set -euo pipefail
apt-get update && apt-get install -y libv8-dev libcurl4-openssl-dev libssl-dev xclip libsasl2-dev zlib1g-dev libfontconfig1-dev libxml2-dev libglpk-dev
r -e 'options(repos = c(CRAN = "https://cloud.r-project.org"))'
r -e "install.packages(c('backports', 'crosstalk', 'clipr' , 'dplyr', 'DT', 'elastic', 'evaluate', 'ggplot2', 'gridExtra', 'highr', 'knitr', 'markdown', 'networkD3', 'rmarkdown', 'rprojroot', 'rsconnect', 'shiny', 'testthat', 'tidyr', 'igraph'), repos='https://cloud.r-project.org')"
r -e "rsconnect::setAccountInfo(name='health-data-hub', token='${TOKEN}', secret='${SECRET}')"
echo "ES_PWD=${ES_PWD}" >> app/server/var_env.txt
echo "ES_HOST=${ES_HOST}" >> app/server/var_env.txt
echo "ES_USERNAME=${ES_USERNAME}" >> app/server/var_env.txt
echo "ES_PORT=${ES_PORT}" >> app/server/var_env.txt
r -e "rsconnect::deployApp(appDir = 'app', appName = 'dico-snds', forceUpdate = TRUE)"

